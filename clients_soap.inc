<?php
/**
 * @file
 * Contains classes for Client connections handlers.
 */

/**
 * Class for client connections, SOAP.
 */
class clients_connection_services_soap extends clients_connection_base {

  // ============================================ Connection form methods.

  /**
   * Declare an array of properties which should be treated as credentials.
   *
   * This lets the credentials storage plugin know which configuration
   * properties to take care of.
   *
   * @return array
   *   A flat array of property names.
   */
  function credentialsProperties() {
    return array('username', 'password');
  }

  /**
   * Extra form elements specific to a class's edit form.
   *
   * @param array $form_state
   *   The form state from the main form, which you probably don't need anyway.
   *
   * @see clients_connection_form()
   * @see clients_connection_form_submit()
   */
  function connectionSettingsFormAlter(&$form, &$form_state) {
     $form['endpoint']['#description'] = t('Remote service URL e.g. http://mysite.com/service-endpoint');

     $form['endpoint']['#maxlength'] = 255;

    $form['configuration'] += array(
      '#type' => 'fieldset',
      '#title' => t('Configuration'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );

    $form['configuration']['login_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Needs login?'),
    );

    $form['configuration']['security_key_needed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Needs security key?'),
    );

    $form['configuration']['security_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Security Key Value'),
      '#size' => 30,
      '#maxlength' => 60,
      '#attributes' => array('autocomplete' => 'off'),
      '#description' => t('This will be added as a parameter to the calls of this webservice.'),
      // '#required' => TRUE,
      '#states' => array(
        'required' => array(
          ':input[name="configuration[security_key_needed]"]' => array('checked' => TRUE),
        ),
        'invisible' => array(
          ':input[name="configuration[security_key_needed]"]' => array('checked' => FALSE),
        ),
        'disabled' => array(
          ':input[name="configuration[security_key_needed]"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['configuration']['security_key_parameter'] = array(
      '#type' => 'textfield',
      '#title' => t('Security Key Parameter Name'),
      '#size' => 30,
      '#maxlength' => 60,
      '#attributes' => array('autocomplete' => 'off'),
      '#description' => t('What is the parameter name to be sended with the key?'),
      // '#required' => TRUE,
      '#states' => array(
        'required' => array(
          ':input[name="configuration[security_key_needed]"]' => array('checked' => TRUE),
        ),
        'invisible' => array(
          ':input[name="configuration[security_key_needed]"]' => array('checked' => FALSE),
        ),
        'disabled' => array(
          ':input[name="configuration[security_key_needed]"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['configuration']['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Service username'),
      '#size' => 30,
      '#maxlength' => 60,
      '#attributes' => array('autocomplete' => 'off'),
      '#description' => t('This should be same as the username on the server you are connecting to.'),
      // '#required' => TRUE,
      '#states' => array(
        'required' => array(
          ':input[name="configuration[login_enabled]"]' => array('checked' => TRUE),
        ),
        'invisible' => array(
          ':input[name="configuration[login_enabled]"]' => array('checked' => FALSE),
        ),
        'disabled' => array(
          ':input[name="configuration[login_enabled]"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['configuration']['password'] = array(
      '#type' => 'password',
      '#title' => t('Service password'),
      '#size' => 30,
      '#maxlength' => 60,
      '#attributes' => array('autocomplete' => 'off'),
      // '#description' => $password_description,
      // '#required' => $password_required,
      '#states' => array(
        // 'required' => array(
        //   ':input[name="configuration[login_enabled]"]' => array('checked' => TRUE),
        // ),
        'invisible' => array(
          ':input[name="configuration[login_enabled]"]' => array('checked' => FALSE),
        ),
        'disabled' => array(
          ':input[name="configuration[login_enabled]"]' => array('checked' => FALSE),
        ),
      ),
    );

    if (isset($this->configuration['password']) && !empty($this->configuration['login_enabled'])) {
      $form['configuration']['password']['#description'] = t('This should be same as the password on the server you are connecting to. Leave blank unless you need to change this.');
    }
    else {
      $form['configuration']['password']['#description'] = t('This should be same as the password on the server you are connecting to.');

      $form['configuration']['password']['#states']['required'] = array(
        ':input[name="configuration[login_enabled]"]' => array('checked' => TRUE),
      );
    }
  }

  /**
   * Submit handler for saving/updating connections of this class.
   *
   * @see clients_connection_form_submit()
   */
  function connectionSettingsForm_submit($form, &$form_state) {
    // This is here to show an example of how this method works.
    parent::connectionSettingsForm_submit($form, $form_state);
  }

  // ============================================ Resource retrieval.

  function connect() {
    $data = array('trace' => 1);
    if (isset($this->configuration['login_enabled']) && !empty($this->configuration['login_enabled'])) {
      // Not sure if this will ever be needed. Check http://bit.ly/1v4qBLl if so
      // Assuming basic auth.
      $data['login'] = $this->configuration['username'];
      $data['password'] = $this->configuration['password'];
    }

    try {
      $client = new \SoapClient($this->endpoint, $data);
    }
    catch (Exception $e) {
      throw new Exception(
        t("Couldn't connect to the Webservice. Received @message",
        array('@message' => $e->getMessage()))
      );
    }

    return $client;
  }

  /**
   * API function to request a remote resource.
   *
   *
   * @param $method
   *  The path of the remote resource to retrieve.
   * @param $method_params
   *  All other parameters are passed to the remote method.
   *
   * @return
   *  Whatever is returned from the remote site.
   */
  function callMethodArray($method, $method_params = array()) {
    $client = $this->connect();
    $result = NULL;
    $debug = FALSE;
    $custom_exception_handler = FALSE;

    if (isset($method_params[0]['debug']) && $method_params[0]['debug']) {
      $debug = TRUE;
      unset($method_params['debug']);
    }
    foreach ($method_params as $key => $method_param) {
      if (isset($method_param['exception_handler']) && $method_param['exception_handler']) {
        $custom_exception_handler = $method_param['exception_handler'];
        if (count($method_params[$key]) == 1) {
          unset($method_params[$key]);
        }
        else {
          unset($method_params[$key]['exception_handler']);
        }
      }
    }

    if (isset($this->configuration['security_key_needed']) && $this->configuration['security_key_needed']) {
      $method_params[0][$this->configuration['security_key_parameter']]
        = $this->configuration['security_key'];
    }
    try {
      $result = $client->__soapCall($method, $method_params);
      if ($debug) {
        $request = $client->__getLastRequest();
        watchdog(
          'clients_soap',
          'Method %method called with the following XML %request',
          array('%method' => $method, '%request' => $request),
          WATCHDOG_NOTICE
        );
      }
    }
    catch (Exception $e) {
      $request = $client->__getLastRequest();
      watchdog(
        'clients_soap',
        'Exception during SOAP call to method %method: %exception - XML generated: %request',
        array('%method' => $method, '%exception' => $e->faultstring, '%request' => $request),
        WATCHDOG_ERROR
      );
      if (!empty($custom_exception_handler)) {
        call_user_func_array ($custom_exception_handler, array($request, $e));
      }
      else {
        /*
        print_r($method_params);
        echo '<hr />';
        var_dump($request, $e);exit;
        */
        $site_email = variable_get('site_mail', '');
        drupal_set_message(
            t('
            Não foi possível se conectar aos serviços acadêmicos.
            Por favor tente novamente em alguns segundos, caso essa mensagem persista por favor contacte o administrador do site pelo e-mail @sitemail', array('@sitemail' => $site_email)
            ), 'error', FALSE);
      }
    }

    return $result;
  }

}
