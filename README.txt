Clients for Drupal Services SOAP Servers
==========================================

This module contains client connection types for connecting to Services module
on Drupal (http://drupal.org/project/services), for endpoints using the SOAP
server.

The remote Drupal site you are connecting to needs Services version 7.x-3.5 or
higher.

Setting up a client
--------------------

At admin/settings/clients, choose the type of connection you want to create.
The Drupal-specific options are as follows:

- Service username, password: the details for a Drupal user on the remote site.

It's a very good idea to go to the test page for your connection and try the
various actions such as logging in and retrieving a node. These show you exactly
what is returned from the remote server.


